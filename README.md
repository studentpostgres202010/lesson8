# Домашнее задание #
### Механизм блокировок ###
Цель: понимать как работает механизм блокировок объектов и строк
***
1. Настройте сервер так, чтобы в журнал сообщений сбрасывалась информация о блокировках, удерживаемых более 200 миллисекунд. Воспроизведите ситуацию, при которой в журнале появятся такие сообщения.

**2020-11-27 05:26:45.310 UTC [15324] postgres@locks LOG:  process 15324 still waiting for ShareLock on transaction 490 after 200000.157 ms**

**2020-11-27 05:26:45.310 UTC [15324] postgres@locks DETAIL:  Process holding the lock: 14436. Wait queue: 15324.**

**2020-11-27 05:26:45.310 UTC [15324] postgres@locks CONTEXT:  while updating tuple (0,5) in relation "accounts"**

**2020-11-27 05:26:45.310 UTC [15324] postgres@locks STATEMENT:  UPDATE accounts SET amount = amount + 1 WHERE id = 1;**

***
2. Смоделируйте ситуацию обновления одной и той же строки тремя командами UPDATE в разных сеансах. Изучите возникшие блокировки в представлении pg_locks и убедитесь, что все они понятны. Пришлите список блокировок и объясните, что значит каждая.

**FROM pg_locks;**

         locktype    | relation | virtxid | xid |       mode       | granted
         ---------------+----------+---------+-----+------------------+---------
         relation      | accounts |         |     | RowExclusiveLock | t
         virtualxid    |          | 7/4     |     | ExclusiveLock    | t
         relation      | accounts |         |     | RowExclusiveLock | t
         virtualxid    |          | 4/17    |     | ExclusiveLock    | t
         relation      | pg_locks |         |     | AccessShareLock  | t
         relation      | accounts |         |     | RowExclusiveLock | t
         virtualxid    |          | 3/13    |     | ExclusiveLock    | t
         transactionid |          |         | 505 | ExclusiveLock    | t
         transactionid |          |         | 507 | ExclusiveLock    | t
         transactionid |          |         | 506 | ExclusiveLock    | t
         transactionid |          |         | 506 | ShareLock        | f
         tuple         | accounts |         |     | ExclusiveLock    | f
         tuple         | accounts |         |     | ExclusiveLock    | t
        (13 rows)

**В первом окне транзакция под номером xid506 заблокировала сроку и вносит изменения, две последовательные транзакции встали в очередь 
появился  тип tuple, после того как транзакция под номером xid506 выполняет comit, xid505 захватывает сроку в режиме  ShareLock вносит изменения 
а 507 все еще ждет своей очереди.**


                transactionid |          |         | 505 | ShareLock        | f
                transactionid |          |         | 505 | ExclusiveLock    | t
                transactionid |          |         | 507 | ExclusiveLock    | t
                tuple         | accounts |         |     | ExclusiveLock    | t


***
3. Воспроизведите взаимоблокировку трех транзакций. Можно ли разобраться в ситуации постфактум, изучая журнал сообщений?

**Да  в принципе в лог файле очень понятно все логируется и прочитав лог можно понять кто какой pid  инициировал взаимоблокировку и какие строки обновлялись.**  

**2020-11-29 12:14:40.255 UTC [4464] postgres@locks ERROR:  deadlock detected**

**2020-11-29 12:14:40.255 UTC [4464] postgres@locks DETAIL:  Process 4464 waits for ShareLock on transaction 534; blocked by process 1602.**

        **Process 1602 waits for ShareLock on transaction 532; blocked by process 4070.**

        **Process 4070 waits for ShareLock on transaction 533; blocked by process 4464.**

        **Process 4464: update accounts set id = 7 where id = 13;**

        **Process 1602: update accounts set id = 10 where id = 4;**

        **Process 4070: update accounts set id = 19 where id = 5;**

***
4. Могут ли две транзакции, выполняющие единственную команду UPDATE одной и той же таблицы (без where), заблокировать друг друга?

**Да могут, если первая транзакция начнет обновлять строки последовательно, а вторая с конца ей на встречу** 

* Попробуйте воспроизвести такую ситуацию.

**DECLARE c CURSOR FOR SELECT * FROM accounts ORDER BY id FOR UPDATe;**

**DECLARE c CURSOR FOR SELECT * FROM accounts ORDER BY id DESC FOR UPDATE;** 

**ERROR:  deadlock detected**

**2020-11-29 12:01:06.772 UTC [4203] postgres@locks DETAIL:  Process 4203 waits for ShareLock on transaction 527; blocked by process 4070.**

        Process 4070 waits for ShareLock on transaction 528; blocked by process 4203.
        Process 4203: FETCH C;
        Process 4070: FETCH C;


***